<?php

namespace App;

use App\Permission;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Change the primary key to UUID
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Disable Auto Increment for primary key
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Allow create tauto imestamps
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'name', 'friendly_name', 'description', 'active'
    ];

    /**
     * Create default values to a new User
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Permission $model) {
            $model->uuid = Str::orderedUuid();
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
    * The Roles that belong to the permission
    */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
