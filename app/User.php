<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    /**
     * Change the primary key to UUID
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Disable Auto Increment for primary key
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Allow create tauto imestamps
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'password', 'remember_token',
    ];

    /**
     * Create default values to a new User
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function (User $model) {
            $model->uuid = Str::orderedUuid();
        });
    }

    /**
     * Set the user's Password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($password)
    {
        if (Hash::needsRehash($password)) {
            $this->attributes['password'] = Hash::make($password);
        }
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
    * The Companies that belong to the user
    */
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

    /**
    * The Roles that belong to the user
    */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
