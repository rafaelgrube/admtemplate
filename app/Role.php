<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * Change the primary key to UUID
     *
     * @var string
     */
    protected $primaryKey = 'uuid';

    /**
     * Disable Auto Increment for primary key
     *
     * @var boolean
     */
    public $incrementing = false;

    /**
     * Allow create tauto imestamps
     *
     * @var boolean
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['name', 'active'];

    /**
     * Create default values to a new User
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Role $model) {
            $model->uuid = Str::orderedUuid();
        });
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /**
    * The Permissions that belong to the role
    */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
    * The Users that belong to the role
    */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
