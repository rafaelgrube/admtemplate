<?php

namespace App;

trait HasRoles
{
    /**
     * Check if the autenticated user is super admin
     *
     * @return boolean
     */
    protected function isSuperAdmin()
    {
        return auth()->user()->id === 1 ?: false;
    }

    /**
     * Check if user has a permission
     *
     * @param string $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
        $permission = Permission::where('name', $permission)
            ->with('roles')
            ->first();

        return $this->hasRole($permission->roles);
    }

    /**
     * Check if autenticated user has a role
     *
     * @param string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }
}
