<?php

namespace App\Http\Controllers\Permission;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::with('roles')
                        ->active()
                        ->get();
        $roles = Role::active()->get();

        return view('permissions.index', compact('permissions', 'roles'));
    }
}
