<?php

namespace App\Http\Controllers\Permission;

use App\Role;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PermissionRoleController extends Controller
{
    /**
     * Get the UUID for all active ROles
     */
    public function getRoles()
    {
        return Role::active()
                ->get()
                ->pluck('uuid');
    }

    /**
     * Associate a role to an permission.
     *
     * @param  App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function syncRoleToPermission(Request $request, Permission $permission)
    {
        $permission->roles()->toggle($request->role_uuid);
        return response()->json($permission->roles);
    }

    /**
     * Attach all roles to an permission.
     *
     * @param  App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function attachAllRolesToPermission(Request $request, Permission $permission)
    {
        $rolesUuid = $this->getRoles();
        $permission->roles()
            ->syncWithoutDetaching($rolesUuid);
        return response()->json($permission->roles);
    }

    /**
     * Attach all roles to an permission.
     *
     * @param  App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function detachAllRolesToPermission(Request $request, Permission $permission)
    {
        $rolesUuid = $this->getRoles();
        $permission->roles()
            ->detach();
        return response()->json($permission->roles);
    }
}
