<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /*
    |--------------------------------------------------------------------------
    | My Custom Logic Based In ResetPasswords Class in Vendor Directory
    |--------------------------------------------------------------------------
    |
    | This methods override methods in the original vendor controller
    |
    */

	public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
			['token' => $token, 'username' => $request->username]
        );
    }

	/**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token'     => 'required',
			'username'  => 'required',
            'password'  => 'required|confirmed|min:6|max:16',
        ];
    }

	/**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
			'username.required'  => 'O Login é obrigatório',
			'password.min'       => 'A senha precisa ter no mínimo 6 caracters.',
			'password.max'       => 'A senha precisa ter no máximo 16 caracters.',
			'password.confirmed' => 'A confirmação da senha não bate.',
		];
    }

	/**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
			'username', 'password', 'password_confirmation', 'token'
        );
    }

	/**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
		return redirect()->back()
	                ->withInput($request->only('username'))
	                ->withErrors(['username' => trans($response)]);
    }

	/**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword(User $user, $password)
    {
		$user->update([
			'password' => $password,
		]);

        $this->guard()->login($user);
    }
}
