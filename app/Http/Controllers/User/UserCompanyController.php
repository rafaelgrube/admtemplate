<?php

namespace App\Http\Controllers\User;

use App\User;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCompanyController extends Controller
{
    /**
     * Get the UUID for all active companies
     */
    public function getCompanies()
    {
        return Company::active()->get()
                ->pluck('uuid');
    }

    /**
     * Associate a company to an user .
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function syncCompanyToUser(Request $request, User $user)
    {
        $user->companies()->toggle($request->company_uuid);
        return response()->json($user->companies);
    }

    /**
     * Attach all companies to an user.
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function attachAllCompaniesToUser(Request $request, User $user)
    {
        $companiesUuid = $this->getCompanies();
        $user->companies()
            ->syncWithoutDetaching($companiesUuid);
        return response()->json($user->companies);
    }

    /**
     * Attach all companies to an user.
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function detachAllCompaniesToUser(Request $request, User $user)
    {
        $companiesUuid = $this->getCompanies();
        $user->companies()
            ->detach();
        return response()->json($user->companies);
    }
}
