<?php

namespace App\Http\Controllers\User;

use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserRoleController extends Controller
{
    /**
     * Get the UUID for all active Roles
     */

    public function getRoles()
    {
        return Role::active()
                ->get()
                ->pluck('uuid');
    }

    /**
     * Associate a role to an user .
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function syncRoleToUser(Request $request, User $user)
    {
        $user->roles()->toggle($request->role_uuid);
        return response()->json($user->roles);
    }

    /**
     * Attach all roles to an user.
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function attachAllRolesToUser(Request $request, User $user)
    {
        $rolesUuid = $this->getRoles();
        $user->roles()
            ->syncWithoutDetaching($rolesUuid);
        return response()->json($user->roles);
    }

    /**
     * Attach all roles to an user.
     *
     * @param  App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function detachAllRolesToUser(Request $request, User $user)
    {
        $rolesUuid = $this->getRoles();
        $user->roles()
            ->detach();
        return response()->json($user->roles);
    }
}
