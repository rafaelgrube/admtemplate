<?php

namespace App\Http\Controllers\Company;

use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::get();
        return view('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.company');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(['name' => 'required|unique:companies|max:255']);
        $company = Company::firstOrCreate($request->except('_token'));
        session()->flash('message', 'Criado com sucesso.');
        return redirect()->route('config.companies.edit', [$company->uuid]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        return response('Access Forbiden', 403);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view('companies.company', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $request->validate(['name' => 'required|max:255|unique:companies,name,'.$company->id]);
        $company->update($request->except('_token'));
        session()->flash('message', 'Atualizado com sucesso.');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        return response('Access Forbiden', 403);
    }

    /**
     * Allow the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function active(Company $company)
    {
        $company->update(['active' => !$company->active]);
        return $company;
    }

    /**
     * Get Active Companies as a resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getActiveCompanies()
    {
        $companies = Company::active()->get();
        return response()->json($companies, 200);
    }
}
