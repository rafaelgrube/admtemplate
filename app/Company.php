<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';
    protected $primaryKey = 'uuid';
    public $incrementing = false;

    protected $fillable = [
        'uuid', 'name', 'corporate_name', 'corporate_number', 'active'
    ];

    public $timestamps = true;

    protected static function boot()
    {
        parent::boot();

        static::creating(function (Company $model) {
            $model->uuid = Str::orderedUuid();
        });
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
