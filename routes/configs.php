<?php

Route::prefix('config')->name('config.')->group(function () {
    // Companies
    Route::get('/companies/get', 'Company\CompanyController@getActiveCompanies');
    Route::put('/companies/{company}/active', 'Company\CompanyController@active')->name('companies.active');
    Route::resource('companies', 'Company\CompanyController');

    // PermissionRoles
    Route::post('/permissions/{permission}/attach-roles', 'Permission\PermissionRoleController@attachAllRolesToPermission');
    Route::post('/permissions/{permission}/detach-roles', 'Permission\PermissionRoleController@detachAllRolesToPermission');
    Route::post('/permissions/{permission}/sync-role', 'Permission\PermissionRoleController@syncRoleToPermission');
    //Permissions
    Route::get('permissions', 'Permission\PermissionController@index')->name('permissions.index');

    // Roles
    Route::get('/roles/get', 'Role\RoleController@getActiveRoles');
    Route::put('/roles/{role}/active', 'Role\RoleController@active')->name('roles.active');
    Route::resource('roles', 'Role\RoleController');

    /**
     * Routes For Users
     */
    // UserCompanies
    Route::post('/users/{user}/attach-companies', 'User\UserCompanyController@attachAllCompaniesToUser');
    Route::post('/users/{user}/detach-companies', 'User\UserCompanyController@detachAllCompaniesToUser');
    Route::post('/users/{user}/sync-company', 'User\UserCompanyController@syncCompanyToUser');
    // UserRoles
    Route::post('/users/{user}/attach-roles', 'User\UserRoleController@attachAllRolesToUser');
    Route::post('/users/{user}/detach-roles', 'User\UserRoleController@detachAllRolesToUser');
    Route::post('/users/{user}/sync-role', 'User\UserRoleController@syncRoleToUser');

    Route::put('/users/{user}/active', 'User\UserController@active')->name('users.active');
    Route::resource('users', 'User\UserController');
});
