<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permission_role', function (Blueprint $table) {
            $table->uuid('permission_uuid');
            $table->uuid('role_uuid');

            $table->primary(['permission_uuid', 'role_uuid']);

            $table->foreign('permission_uuid')
                ->references('uuid')->on('permissions')
                ->onDelete('cascade');

             $table->foreign('role_uuid')
                ->references('uuid')->on('roles')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permission_role');
    }
}
