<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::firstOrCreate([
            'name' => 'show-config-menu',
            'friendly_name' => 'Exibe Menu de Configurações',
            'description' => 'Exibe o menu Configurações no menu lateral',
        ]);
        Permission::firstOrCreate([
            'name' => 'show-companies-menu',
            'friendly_name' => 'Exibe Menu de Empresas',
            'description' => 'Exibe o menu Administração de Empresas no menu lateral',
        ]);
        Permission::firstOrCreate([
            'name' => 'show-roles-menu',
            'friendly_name' => 'Exibe Menu de Grupos',
            'description' => 'Exibe o menu Administração de Grupos no menu lateral',
        ]);
        Permission::firstOrCreate([
            'name' => 'show-users-menu',
            'friendly_name' => 'Exibe Menu de Usuários',
            'description' => 'Exibe o menu Administração de Usuário no menu lateral',
        ]);
        Permission::firstOrCreate([
            'name' => 'show-permissions-menu',
            'friendly_name' => 'Exibe Menu de Permissiões',
            'description' => 'Exibe o menu Administração de Permissões no menu lateral',
        ]);
    }
}
