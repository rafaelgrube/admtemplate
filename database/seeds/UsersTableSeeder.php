<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(
            [
                'username' => 'rgrube'
            ],
            [
                'name' => 'Rafael Grube',
                'email' => 'rafaelgrube@gmail.com',
                'password' => Hash::make('barakinha'),
            ]
        );
    }
}
