<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple labels. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'username' => 'Login',
    'password' => 'Senha',
    'remember_me' => 'Mantenha-me conectado',
    'login' => 'Entrar',
    'email' => 'Endereço de Email',
    'password.reset.email' => 'Enviar link de troca de senha',
    'password.forgot' => 'Esqueci minha senha?',
    'password.confirmation' => 'Confirmar Senha',
    'password.reset' => 'Alterar Senha'
];
