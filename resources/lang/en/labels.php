<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple labels. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'username' => 'Login',
    'password' => 'Password',
    'remember_me' => 'Remember Me',
    'login' => 'Login',
    'email' => 'E-mail Address',
    'pasword.reset.email' => 'Send Password Reset Link',
    'password.forgot' => 'Forgot Your Password?',
    'password.confirmation' => 'Confirm Password',
    'password.reset' => 'Reset Password'
];
