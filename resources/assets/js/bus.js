/* ------------------------------------------------ *
 *		This is a global component to share
 * 		states betweem components Vue.
 * ------------------------------------------------ */

import Vue from 'vue'
export default new Vue()
