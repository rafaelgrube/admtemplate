
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */



require('./bootstrap');

window.Vue = require('vue');


/**
* This is required for the third party custom components
*/
// Select as Custom Dropdown
require('semantic-ui-transition/transition')
require('semantic-ui-dropdown/dropdown')
require('semantic-ui-popup/popup')
require('jquery-tablesort/jquery.tablesort')

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('alerts', require('./components/Alerts.vue'))
Vue.component('sidebar', require('./components/Sidebar.vue'))

Vue.component('companies-table', require('./components/Company/CompaniesTable.vue'))
Vue.component('permissions-table', require('./components/Permissions/PermissionsTable.vue'))
Vue.component('permission-roles', require('./components/Permissions/ModalSyncRolesToPermission.vue'))
Vue.component('roles-table', require('./components/Roles/RolesTable.vue'))
Vue.component('users-table', require('./components/User/UsersTable.vue'))
Vue.component('user-companies', require('./components/User/UserCompaniesModal.vue'))
Vue.component('user-roles', require('./components/User/UserRolesModal.vue'))

const app = new Vue({
    el: '#app'
});
