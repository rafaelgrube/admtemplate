@extends('layouts.adm')
@section('content')
<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <a class="breadcrumb-item"
        href="{{ route('config.roles.index') }}">
        Grupos
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Grupo</div>
</div>

<div class="container">
    <form method="POST"
        class=""
        action="{{
            isset($role)
            ? route('config.roles.update', $role)
            : route('config.roles.store')
        }}">

        @csrf
        @if(isset($role))
            @method('PUT')
        @endif

         <div class="card">
            <h5 class="card-header">
                Informações do Grupo
            </h5>
            <div class="card-body">
                <div class="form-group">
                    <label for="roleName"
                        class="required">
                        Nome
                    </label>
                    <input type="text"
                        name="name"
                        id="roleName"
                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                        value="{{ $role->name or old('name') }}"
                        placeholder="Nome da Empresa" />
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col text-right">
                <button type="submit"
                    class="btn btn-success px-4 py-2 rounded-full hover:shadow-md"
                    role="button"
                    tabindex="0">
                    Salvar Grupo
                </button>
            </div>
        </div>
    </form>
</div>
@endsection