@extends('layouts.adm')
@section('content')

<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Usuários</div>
</div>

<div class="container">
    <users-table users="{{ $users }}"></users-table>
</div>
@endsection