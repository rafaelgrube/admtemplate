@extends('layouts.adm')
@section('content')
<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <a class="breadcrumb-item"
        href="{{ route('config.users.index') }}">
        Usuários
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Usuário</div>
</div>

<div class="container">
    <form method="POST"
        class=""
        action="{{
            isset($user)
            ? route('config.users.update', $user)
            : route('config.users.store')
        }}">

        @csrf
        @if(isset($user))
            @method('PUT')
        @endif

        <div class="card">
            <h5 class="card-header">
                Informações do Usuário
            </h5>
            <div class="card-body">
                <div class="form-group">
                    <label for="userName"
                        class="required">
                        Nome
                    </label>
                    <input type="text"
                        name="name"
                        id="userName"
                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                        value="{{ $user->name or old('user') }}"
                        placeholder="Nome do Usuário"
                        min="3"
                        max="255"
                        required
                        autofocus />
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="userEmail"
                        class="required">
                        Email
                    </label>
                    <input type="email"
                        name="email"
                        id="userEmail"
                        class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                        value="{{ $user->email or old('email') }}"
                        placeholder="Email do Usuário"
                        min="3"
                        max="255"
                        required />
                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="userUsername"
                        class="required">Login</label>
                    <input type="text"
                        name="username"
                        id="userUsername"
                        class="form-control {{ $errors->has('username') ? ' is-invalid' : '' }}"
                        value="{{ $user->username or old('username') }}"
                        placeholder="Login do Usuário"
                        min="3"
                        max="255"
                        required />
                    @if ($errors->has('username'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
               <div class="form-row">
                    <div class="col form-group mr-2">
                        <label for="userPassword"
                            class="{{ !isset($user) ? 'required' : null }}">
                            Senha
                        </label>
                        <input type="password"
                            name="password"
                            id="userPassword"
                            class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                            value="{{ old('password') }}"
                            min="3"
                            max="255"
                            placeholder="Senha para o usuário"
                            {{ !isset($user) ? 'required' : null }}
                            minlength="6"
                            maxlength="255" />
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col form-group ml-2">
                        <label for="userPasswordConfirmation"
                            class="{{ !isset($user) ? 'required' : null }}">
                            Confirmar Senha
                        </label>
                        <input type="password"
                            name="password_confirmation"
                            id="userPasswordConfirmation"
                            class="form-control"
                            value="{{ old('password_confirmation') }}"
                            placeholder="Confirma a senha do usuário"
                            {{ !isset($user) ? 'required' : null }}
                            minlength="6"
                            maxlength="255" />
                    </div>
                </div>
            </div>
        </div>

         <div class="d-flex justify-content-end mt-2">
            @can('show-companies-menu')
                <user-companies user="{{ $user or null }}"
                    companies="{{ $user->companies or null }}"
                    class="mr-2">
                </user-companies>
            @endcan
            @can('show-roles-menu')
                <user-roles user="{{ $user or null }}"
                    roles="{{ $user->roles or null }}"
                    class="mr-2">
                </user-roles>
            @endcan
            <button type="submit"
                class="btn btn-success px-4 py-2 rounded-full hover:shadow-md"
                role="button">
                Salvar Usuário
            </button>
        </div>
    </form>
</div>
@endsection