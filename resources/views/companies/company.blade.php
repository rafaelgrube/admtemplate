@extends('layouts.adm')
@section('content')
<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <a class="breadcrumb-item"
        href="{{ route('config.companies.index') }}">
        Empresas
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Empresa</div>
</div>

<div class="container">

    <form method="POST"
        class=""
        action="{{
            isset($company)
            ? route('config.companies.update', $company)
            : route('config.companies.store')
        }}">

        @csrf
        @if(isset($company))
            @method('PUT')
        @endif

         <div class="card mb-3">
            <h5 class="card-header">
                Informações da empresa
            </h5>
            <div class="card-body">
                <div class="form-group">
                    <label for="companyName"
                        class="required">Nome</label>
                    <input type="text"
                        name="name"
                        id="companyName"
                        class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                        value="{{ $company->name or old('name') }}"
                        placeholder="Nome da Empresa" />
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="corporateName">
                        Razão Social
                    </label>
                    <input type="text"
                        name="corporate_name"
                        id="corporateName"
                        class="form-control {{ $errors->has('corporate_name') ? ' is-invalid' : '' }}"
                        value="{{ $company->corporate_name or old('corporate_name') }}"
                        placeholder="Razão Social da Empresa" />
                    @if ($errors->has('corporate_name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('corporate_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="corporateName">CNPJ</label>
                    <input type="text"
                        name="corporate_number"
                        id="corporateName"
                        class="form-control {{ $errors->has('corporate_number') ? ' is-invalid' : '' }}"
                        value="{{ $company->corporate_number or old('corporate_number') }}"
                        placeholder="CNPJ" />
                    @if ($errors->has('corporate_number'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('corporate_number') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="text-right">
            <button type="submit"
                class="btn btn-success px-4 py-2 rounded-full hover:shadow-md"
                role="button"
                tabindex="0">
                Salvar Empresa
            </button>
        </div>
    </form>
</div>
@endsection