@extends('layouts.adm')
@section('content')
<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Empresas</div>
</div>

<div class="container">
    <companies-table companies="{{ $companies }}"></companies-table>
</div>
@endsection