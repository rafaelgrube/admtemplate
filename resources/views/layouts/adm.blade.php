<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/css/iziToast.min.css">
    <body>
        <div id="app" v-cloak>
            <header>
                <div class="header-wrapper">
                    <div class="header-left">
                        <div class="d-flex justify-content-center align-items-center">
                            FleetChange
                        </div>
                    </div>

                    <div class="header-center px-4">
                        <div class="flex align-items-center justify-content-between">
                            <div class="relative">
                            </div>
                        </div>
                    </div>

                    <div class="header-right">
                        @auth
                        <div class="dropdown-toggle d-flex align-items-center" id="dropdownMenuHeaderButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false" role="button" style="cursor: pointer;">

                            @if(!empty(auth()->user()->info->avatar))
                            <img src="{{ Storage::disk('avatar')->url(auth()->user()->info->avatar) }}" class="avatar" alt="" />                            @else {!! Avatar::create(auth()->user()->name)->setDimension(36, 36)->setFontSize(16)->toSvg();
                            !!} @endif

                            <div class="d-inline-block ml-2">
                                {{ auth()->user()->name }}
                            </div>
                        </div>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuHeaderButton">
                            <a class="dropdown-item" href="{{ route('config.users.edit', auth()->user()->uuid) }}" style="cursor: pointer;">
									Editar minhas informações
								</a>
                            <div class="dropdown-divider"></div>
                            <div>
                                <a onclick="event.preventDefault();
											document.getElementById('logout-form')
											.submit();" href="{{ url('/logout') }}" class="dropdown-item">
										Sair
									</a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </div>
                        @endauth
                    </div>

                    <div class="header-menu-mobile">
                        <span class="btn-open-menu-mobile">Menu</span>
                    </div>
                </div>
            </header>

            <alerts></alerts>

            <div class="main-container" id="main-container">
                <sidebar>
                    @include('layouts.components.sidebar')
                </sidebar>
                <div id="content" class="container-fluid main-content">
                    @yield('content')
                </div>
            </div>
        </div>

        <script src="{{ mix('/js/app.js') }}"></script>
        <script>
            $(() => {
				openSidebarMobile()
				closeSidebarMobile()
				checkIfMobile()

				$('.selectbox').dropdown();
				$('table').tablesort();
				$('[data-toggle="tooltip"]').tooltip()
			})

			function checkIfMobile() {
				if($(window).width() < 768) {
					if($('aside').hasClass('is-open')){
						$('aside').removeClass('is-open')
					}
				}
			}

			function openSidebarMobile()
			{
				$('.btn-open-menu-mobile').on('click', function () {
					$('aside').addClass('is-open')
				})
			}

			function closeSidebarMobile()
			{
				$('.btn-close-menu-mobile').on('click', function() {
					$('aside').removeClass('is-open')
				})
			}
        </script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.3.0/js/iziToast.min.js"></script>
        @include('layouts.components.alerts')
        @stack('scripts')
    </body>

</html>