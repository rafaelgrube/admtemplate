@if(session()->has('message'))
    <script>
        iziToast.show({
            title: 'Sucesso',
            message: "{{ session()->get('message') }}",
            color: 'green',
            position: 'topRight',
        })
    </script>
@endif
@if($errors->count() > 0)
    @foreach($errors->all() as $error)
        <script>
            iziToast.show({
                title: 'Erro',
                message: "{{ $error }}",
                color: 'red',
                position: 'topRight',
            })
        </script>
    @endforeach
@endif