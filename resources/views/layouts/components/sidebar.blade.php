<a href="/" class="item {{
        request()->is('/')
            ? 'active'
            : null
        }}" role="button">
    <i class="fas fa-home icon"></i>
    <div class="text">Home</div>
</a>

@can('show-config-menu')
    <a class="item item-collapse"
        data-toggle="collapse"
        href="#collapseConfig"
        role="button"
        aria-expanded="{{
                request()->is('config/*')
                    ? 'true'
                    : 'false'
                }}"
        aria-controls="collapseConfig">
        <i class="fas fa-cogs icon"></i>
        <div class="text">Configurações</div>
    </a>

    <div class="collapse {{ request()->is('config/*') ? 'show' : null }}" id="collapseConfig">
        <!-- Clients -->
        @can('show-config-menu')
            <a href="{{ route('config.companies.index') }}"
                class="item {{ request()->is('config/companies*') ? 'active' : null }}"
                role="button">
                <i class="fas fa-address-book icon"></i>
                <div class="text">Empresas</div>
            </a>
        @endcan
        <!-- Roles -->
        @can('show-roles-menu')
            <a href="{{ route('config.roles.index') }}"
                class="item {{ request()->is('config/roles*') ? 'active' : null }}"
                role="button">
                <i class="fas fa-ruler icon"></i>
                <div class="text">Grupos</div>
            </a>
        @endcan
        <!-- Users -->
        @can('show-users-menu')
            <a href="{{ route('config.users.index') }}"
                class="item {{ request()->is('config/users*') ? 'active' : null }}"
                role="button">
                <i class="fas fa-address-card icon"></i>
                <div class="text">Usuários</div>
            </a>
        @endcan
        <!-- Permissions -->
        @can('show-permissions-menu')
            <a href="{{ route('config.permissions.index') }}"
                class="item {{ request()->is('config/permissions*') ? 'active' : null }}"
                role="button">
                <i class="fas fa-user-shield icon"></i>
                <div class="text">Permissões</div>
            </a>
        @endcan
    </div>
@endcan