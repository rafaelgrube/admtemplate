@extends('layouts.adm')
@section('content')
<div class="breadcrumbs">
    <a class="breadcrumb-item"
        href="{{ route('home') }}">
        Home
    </a>
    <i class="fas fa-angle-right icon"></i>
    <div class="active breadcrumb-item">Permissões</div>
</div>

<div class="container">
    <permissions-table
        :permissions="{{ $permissions }}"
        :roles="{{ $roles }}">
    </permissions-table>
</div>
@endsection